// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("Base2art.Bob.Coordinator")]
[assembly: AssemblyVersion("0.0.0.1")]
[assembly: AssemblyFileVersion("0.0.0.1")]
[assembly: ComVisible(false)]
