﻿namespace Base2art.Bob.Coordinator
{
    using System;
    
    public class ProjectData
    {
        private readonly Guid correlationId;
        
        private readonly string branchOrTag;
        
        private readonly string projectName;
        
        public ProjectData(Guid correlationId, string projectName, string branchOrTag)
        {
            this.correlationId = correlationId;
            this.projectName = projectName;
            this.branchOrTag = branchOrTag;
        }
        
        public Guid CorrelationId 
        {
            get { return this.correlationId; }
        }
        
        public string ProjectName 
        {
            get { return this.projectName; }
        }
        
        public string BranchOrTag 
        {
            get { return this.branchOrTag; }
        }
    }
}
