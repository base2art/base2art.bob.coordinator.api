﻿namespace Base2art.Bob.Coordinator
{
    public interface IExecutionResult
    {
        ExecutionStatus Status { get; }
    }
}
