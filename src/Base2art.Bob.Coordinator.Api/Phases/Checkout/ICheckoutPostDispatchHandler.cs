﻿namespace Base2art.Bob.Coordinator
{
    public interface ICheckoutPostDispatchHandler
    {
        bool CanHandlePost(
            ILogger logger,
            ProjectData projectData,
            CheckoutPostExecutionData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult HandlePost(
            ILogger logger,
            ProjectData projectData,
            CheckoutPostExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}
