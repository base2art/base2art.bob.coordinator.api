﻿namespace Base2art.Bob.Coordinator
{
    public interface ICheckoutPreDispatchHandler
    {
        bool CanHandlePre(
            ILogger logger,
            ProjectData projectData,
            CheckoutPreExecutionData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult HandlePre(
            ILogger logger,
            ProjectData projectData,
            CheckoutPreExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}
