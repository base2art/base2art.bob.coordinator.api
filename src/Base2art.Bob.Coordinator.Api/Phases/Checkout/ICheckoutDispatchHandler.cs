﻿namespace Base2art.Bob.Coordinator
{
    public interface ICheckoutDispatchHandler
    {
        bool CanHandle(
            ILogger logger,
            ProjectData projectData,
            CheckoutDispatchHandlerData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult Handle(
            ILogger logger,
            ProjectData projectData,
            CheckoutDispatchHandlerData executionData,
            CheckoutResultData resultData,
            ExecutionStatus executionStatus);
    }
}
