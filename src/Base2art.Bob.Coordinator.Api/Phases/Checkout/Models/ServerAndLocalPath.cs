﻿namespace Base2art.Bob.Coordinator
{
    public class ServerAndLocalPath
    {
        public string Server { get; set; }

        public string LocalPath { get; set; }
    }
}
