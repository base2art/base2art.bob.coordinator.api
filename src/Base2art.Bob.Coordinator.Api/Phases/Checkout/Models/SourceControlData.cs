﻿namespace Base2art.Bob.Coordinator.Phases.Checkout.Models
{
    public class SourceControlData
    {
        private readonly string sourceControlType;
        
        private readonly string sourceControlLocation;
        
        private readonly string userName;
        
        private readonly string password;
        
        public SourceControlData(
            string sourceControlType,
            string sourceControlLocation,
            string userName,
            string password)
        {
            this.sourceControlType = sourceControlType;
            this.sourceControlLocation = sourceControlLocation;
            this.userName = userName;
            this.password = password;
        }
        
        public string SourceControlType
        {
            get { return this.sourceControlType; }
        }

        public string SourceControlLocation
        {
            get { return this.sourceControlLocation; }
        }

        public string UserName
        {
            get { return this.userName; }
        }

        public string Password
        {
            get { return this.password; }
        }
    }
}
