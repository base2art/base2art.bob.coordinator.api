﻿namespace Base2art.Bob.Coordinator
{
    using System.Linq;
    using Base2art.Bob.Coordinator.Phases.Checkout.Models;

    public class CheckoutPreExecutionData
    {
        private readonly SourceControlData[] locations;

        public CheckoutPreExecutionData(SourceControlData[] locations)
        {
            this.locations = locations;
        }
        
        public SourceControlData[] CheckoutLocations
        {
            get
            {
                return this.locations
                    .Select(x => new SourceControlData(x.SourceControlType, x.SourceControlLocation, x.UserName, x.Password))
                    .ToArray();
            }
        }
    }
}
