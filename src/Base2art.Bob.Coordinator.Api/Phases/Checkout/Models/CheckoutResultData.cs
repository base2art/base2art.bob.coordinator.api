﻿namespace Base2art.Bob.Coordinator
{
    using System.Collections.Generic;
    using System.Linq;
    
    public class CheckoutResultData
    {
        private readonly List<ServerAndLocalPath> items = new List<ServerAndLocalPath>();
        
        public ServerAndLocalPath[] CheckoutLocations
        {
            get 
            { 
                return this.items
                    .Select(x => new ServerAndLocalPath { Server = x.Server, LocalPath = x.LocalPath })
                    .ToArray();
            }
        }
        
        public ServerAndLocalPath AddCheckoutLocation(string server, string localPath)
        {
            if (string.IsNullOrWhiteSpace(server) || string.IsNullOrWhiteSpace(localPath))
            {
                return null;
            }
            
            var serverAndLocalPath = new ServerAndLocalPath 
            {
                Server = server,
                LocalPath = localPath
            };
            
            this.items.Add(serverAndLocalPath);
            
            return serverAndLocalPath;
        }
    }
}
