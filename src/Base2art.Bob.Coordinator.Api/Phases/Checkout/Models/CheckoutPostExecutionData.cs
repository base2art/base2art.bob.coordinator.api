﻿namespace Base2art.Bob.Coordinator
{
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Bob.Coordinator.Phases.Checkout.Models;

    public class CheckoutPostExecutionData
    {
        private readonly IEnumerable<KeyValuePair<SourceControlData, CheckoutLocation[]>> data;

        public CheckoutPostExecutionData(List<KeyValuePair<SourceControlData, CheckoutLocation[]>> data)
        {
            this.data = data ?? new List<KeyValuePair<SourceControlData, CheckoutLocation[]>>();
        }
        
        public IList<KeyValuePair<SourceControlData, CheckoutLocation[]>> Data
        {
            get { return new List<KeyValuePair<SourceControlData, CheckoutLocation[]>>(this.data); }
        }
    }
}
