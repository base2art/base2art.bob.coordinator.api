﻿namespace Base2art.Bob.Coordinator
{
    public interface IArtifactPublisherDispatchHandler
    {
        bool CanHandle(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherDispatchHandlerData executionData,
            ExecutionStatus executionStatus);

        IDispatchResult Handle(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherDispatchHandlerData executionData,
            ArtifactPublisherResultData resultData,
            ExecutionStatus executionStatus);
    }
}
