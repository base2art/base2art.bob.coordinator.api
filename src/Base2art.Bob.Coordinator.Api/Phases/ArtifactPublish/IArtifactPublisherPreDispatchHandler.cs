﻿namespace Base2art.Bob.Coordinator
{
    public interface IArtifactPublisherPreDispatchHandler
    {
        bool CanHandlePre(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherPreExecutionData executionData,
            ExecutionStatus executionStatus);

        IDispatchResult HandlePre(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherPreExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}


