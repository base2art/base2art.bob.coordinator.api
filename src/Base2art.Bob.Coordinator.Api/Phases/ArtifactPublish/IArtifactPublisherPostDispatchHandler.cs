﻿namespace Base2art.Bob.Coordinator
{
    public interface IArtifactPublisherPostDispatchHandler
    {
        bool CanHandlePost(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherPostExecutionData executionData,
            ExecutionStatus executionStatus);

        IDispatchResult HandlePost(
            ILogger logger,
            ProjectData projectData,
            ArtifactPublisherPostExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}


