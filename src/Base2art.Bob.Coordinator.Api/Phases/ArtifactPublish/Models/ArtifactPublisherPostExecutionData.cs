﻿namespace Base2art.Bob.Coordinator
{
	using System.Linq;
	using Base2art.Bob.Coordinator.Phases.Build.Models;

	public class ArtifactPublisherPostExecutionData
	{
		private readonly ArtifactDirectory[] outputPaths;

		public ArtifactPublisherPostExecutionData(ArtifactDirectory[] outputPaths)
		{
			this.outputPaths = outputPaths ?? new ArtifactDirectory[0];
		}

		public ArtifactDirectory[] OutputPaths 
		{
			get { return this.outputPaths.Select(x => new ArtifactDirectory(x.Value)).ToArray(); }
		}
	}
}


