﻿namespace Base2art.Bob.Coordinator
{
    public class ArtifactPublisherDispatchHandlerData
    {
        private readonly string artifactPath;

        public ArtifactPublisherDispatchHandlerData(string artifactPath)
        {
            this.artifactPath = artifactPath;
        }
        
        public string ArtifactPath
        {
            get { return this.artifactPath; }
        }
    }
}
