﻿namespace Base2art.Bob.Coordinator
{
    public interface ICleanupPostDispatchHandler
    {
        bool CanHandlePost(
            ILogger logger,
            ProjectData projectData,
            CleanupPostExecutionData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult HandlePost(
            ILogger logger,
            ProjectData projectData,
            CleanupPostExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}
