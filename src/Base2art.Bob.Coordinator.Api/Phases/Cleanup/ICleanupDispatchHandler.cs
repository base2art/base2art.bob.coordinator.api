﻿namespace Base2art.Bob.Coordinator
{
    public interface ICleanupDispatchHandler
    {
        bool CanHandle(
            ILogger logger,
            ProjectData projectData,
            CleanupDispatchHandlerData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult Handle(
            ILogger logger,
            ProjectData projectData,
            CleanupDispatchHandlerData executionData,
            CleanupResultData resultData,
            ExecutionStatus executionStatus);
    }
}
