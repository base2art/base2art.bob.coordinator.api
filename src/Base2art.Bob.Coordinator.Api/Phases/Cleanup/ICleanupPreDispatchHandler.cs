﻿namespace Base2art.Bob.Coordinator
{
    public interface ICleanupPreDispatchHandler
    {
        bool CanHandlePre(
            ILogger logger,
            ProjectData projectData,
            CleanupPreExecutionData executionData,
            ExecutionStatus executionStatus);
        
        IDispatchResult HandlePre(
            ILogger logger,
            ProjectData projectData,
            CleanupPreExecutionData executionData,
            ExecutionStatus executionStatus);
    }
}
