﻿namespace Base2art.Bob.Coordinator
{
    public interface ILogger
    {
        void Info(string value);
        
        void Info<T>(T obj) where T : class;
        
        void Debug(string value);
        
        void Debug<T>(T obj) where T : class;
        
        void Error(string value);
        
        void Error<T>(T obj) where T : class;
    }
}
