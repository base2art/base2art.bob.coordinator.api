﻿namespace Base2art.Bob.Coordinator
{
    public interface IDispatchResult
    {
        bool HaltExecution { get; }
        
        bool? IsSuccess { get; }
    }
}
