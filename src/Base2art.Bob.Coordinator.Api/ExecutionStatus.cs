﻿namespace Base2art.Bob.Coordinator
{
    public enum ExecutionStatus
    {
        Inconclusive = 0,
        Success = 1,
        Failure = 2,
    }
}
